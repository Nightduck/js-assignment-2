# Assignment 2 React

Deployed on Heroku: https://lost-in-translation3.herokuapp.com/  
The project was successfully deployed to Heroku, but stopped working after a while...  
We are not sure how to fix this, but it works as intended when you:  
* Clone repository
* Open terminal on root folder (/js-assignment-2)
* Type "npm install"
* Type "npm run dev"

This should open your browser on the login page in the app.


### Participants
* Martin Mikkelsen
* Isar Buzza
* Sander Island

## Description
This project is a group project for a React-assignment in Noroff's Java Fullstack course. In this project, we have created a database consisting of movies, characters and franchises, and our task is to extract and alter/update desired data. In this project, we have designed a ASL (American Sign Language) web page using React, TypeScript, HTML and CSS. The web page is built as a SPA (Single Page Application).

## Project task
The application will have one main feature: to act as a “translator” from regular text to sign language. The application
must be able to translate English words and short sentences to American sign language.

The first thing a user should see is the “Login page” where the user must be able to enter their name.
Save the username to the Translation API. Remember to first check if the user exists. Once the name has been stored in
the API, the app must display the main page, the translation page.
Users that are already logged in may automatically be redirected to the Translation page. You may use the browsers’ local
storage to manage the session. 

A user may only view this page if they are currently logged into the app. Please redirect a user back to the login page if
now active login session exists in the browser storage.
The user types in the input box at the top of the page. The user must click on the “translate” button to the right of the
input box to trigger the translation.
Translations must be stored using the API (See Required features for more information). The Sign language characters
must appear in the “translated” box. You may choose to limit the input to 40 letters. (Not required).
You may ignore special characters and spaces.

The profile page must display the last 10 translations for the current user. You only need to display the text of the
translation.
There must also be a button to clear the translations. This should “delete” in your API and no longer display on the profile
page. To simplify things, you may simply delete the records, but you should NEVER delete data from a database.
The Logout button should clear all the storage and return to the start page. You may design this however you’d like.

The application shoud be deployed on Heroku.

### Pages
#### Login Page
**Endpoint:** /login  
This page is where the user ends up if there is no user logged in. It is a simple and straight forward web page where a user
types in a user name and logs in.

#### Translate Page
**Endpoint:** /translate  
This page is where the user can translate a word or phrase. The output box will display the translated content in a clear manner.

#### Profile Page
**Endpoint:** /profile  
The profile page displays the last ten translations that the user has done.

### Tools
* Visual Studio Code has been used for coding
* Git has been used for version control asnd storage on Gitlab
* React framework has been used
* Figma has been used for designing wireframe
* The program is written in TypeScript
* HTML and CSS is used for webdesign
* Heroku is used for deployment

### Git usage
We had our own branches where each group member worked on their given tasks.
