import "../styles/Login.css";
import TranslationHistory from "../components/TranslationHistory";
import { LogoutButton } from "../components/LogoutButton";

export const Profile = () => {
  return (
    <div id="outerDiv">
      <TranslationHistory />
      <LogoutButton />
    </div>
  );
};

export default Profile;
