import { Card } from "react-bootstrap";
import { TranslationForm } from "../components/TranslationForm";
import "../styles/Translate.css";
import { stringToSigns } from "../utils/translator";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
import { useSelector } from "react-redux";

export const Translate = () => {
  const username = useSelector((state: any) => state.userReducer.username);
  const userHistory = useSelector((state: any) => state.userReducer.history);

  const navigate = useNavigate();
  useEffect(() => {
    if (!username) {
      navigate("/login");
    }
  });

  let data: string[] = [];
  if (userHistory.length > 0) {
    data = stringToSigns(userHistory.at(-1));
  }

  return (
    <div id="translate-container">
      <Card className="translate-card">
        <Card.Title className="translation-title">
          Sign Language Translator
        </Card.Title>
        <TranslationForm width={20} height={3} />
        <Card id="translation-output-card">
          <>
            {data?.map((sign: string) => {
              return (
                <div key={data.indexOf(sign)}>
                  <Card.Img className="sign-image" src={sign} />
                </div>
              );
            })}
          </>
        </Card>
      </Card>
    </div>
  );
};

export default Translate;
