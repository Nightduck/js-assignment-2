import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import "../styles/App.css";
import { PageNavbar } from "../components/PageNavbar";
import { Provider } from "react-redux";
import { store } from "../store/store";
import Translate from "./Translate";
import Login from "./Login";
import Profile from "./Profile";
import ErrorPage from "./ErrorPage";

function App() {
  return (
    <div className="App">
      <Provider store={store}>
        <BrowserRouter>
          <PageNavbar />
          <Routes>
            <Route path="/" element={<Login />} />
            <Route path="translate" element={<Translate />} />
            <Route path="profile" element={<Profile />} />
            <Route path="login" element={<Login />} />
            <Route path="*" element={<ErrorPage />} />
          </Routes>
        </BrowserRouter>
      </Provider>
    </div>
  );
}

export default App;
