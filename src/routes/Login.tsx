import { LoginForm } from "../components/LoginForm";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
import { Row } from "react-bootstrap";
import "../styles/Login.css";

export const Login = () => {
  const username = useSelector((state: any) => state.userReducer.username);

  const navigate = useNavigate();
  useEffect(() => {
    if (username) {
      navigate("/translate");
    }
  });

  return (
    <div id="outerDiv">
      <Row id="row" align="center">
        <div id="inputBox">
          <div id="inputField">
            <h3>Enter username to login</h3>
            <LoginForm width={20} height={3} />
          </div>
        </div>
      </Row>
    </div>
  );
};

export default Login;
