export function ErrorPage() {
  return (
    <div id="error-page">
      <h1>404 Not Found</h1>
      <p>Sorry, an unexpected error has occurred.</p>
    </div>
  );
}

export default ErrorPage;
