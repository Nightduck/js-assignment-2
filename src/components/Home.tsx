import React from "react";
import { Card } from "react-bootstrap";
import stringToSigns from "../utils/translator";
import "../styles/Home.css";

export const Home: React.FC = () => {
  const welcome = stringToSigns("welcome");

  return (
    <div id="main">
      <Card id="home-card">
        <Card.Title>Lost In Translation</Card.Title>
        <Card.Body>
          <div className="welcome-signs">
            <>
              {welcome.map((sign: string) => {
                return (
                  <div>
                    <Card.Img className="sign-image" src={sign} />
                  </div>
                );
              })}
            </>
          </div>
          <Card.Text>
            Welcome the the Lost In Translation ASL Translator!
          </Card.Text>
          <Card.Text>
            Here, you can translate short words and sentences to see they are
            spelled using ASL.
          </Card.Text>
        </Card.Body>
      </Card>
    </div>
  );
};
