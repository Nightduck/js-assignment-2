import React from "react";
import { Container, Nav, Navbar } from "react-bootstrap";
import { NavbarUser } from "./NavbarUser";
import "../styles/Navbar.css";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";

export const PageNavbar: React.FC = () => {
  const username = useSelector((state: any) => state.userReducer.username);
  console.log(username);

  return (
    <Navbar id="navbar" variant="dark">
      <Container id="navbar-container">
        <Navbar.Brand as={Link} id="brand" to="/">
          Lost in Translation
        </Navbar.Brand>
        <Nav className="me-auto">
          <Nav.Link as={Link} to="/translate">
            Translate
          </Nav.Link>
          <Nav.Link as={Link} to="/profile">
            Profile
          </Nav.Link>
        </Nav>
        <NavbarUser user={username} />
      </Container>
    </Navbar>
  );
};
