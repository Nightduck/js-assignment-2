import React from "react";
import Card from "react-bootstrap/esm/Card";
import "../styles/TranslationHistory.css";
import "../utils/userHandling";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
import { Button } from "react-bootstrap";
import { length } from "localforage";
import { clearHistory } from "../store/userSlice";
import { addTranslation } from "../utils/userHandling";
import { render } from "react-dom";

export const TranslationHistory: React.FC = () => {
  const username = useSelector((state: any) => state.userReducer.username);
  let userHistory = useSelector((state: any) => state.userReducer.history);
  const dispatch = useDispatch();

  const navigate = useNavigate();
  useEffect(() => {
    if (!username) {
      navigate("/login");
    }
  });

  function printHistory() {
    if (userHistory.length === 0) {
      console.log("Translate history is empty");

      return (
        <Card.Text className="history-entry-empty">
          Translate something to fill history
        </Card.Text>
      );
    } else {
      console.log("Translate history is not empty");
      console.log(userHistory);

      return (
        <>
          <Card.Text className="history-entry">{userHistory[0]}</Card.Text>
          <Card.Text className="history-entry">{userHistory[1]}</Card.Text>
          <Card.Text className="history-entry">{userHistory[2]}</Card.Text>
          <Card.Text className="history-entry">{userHistory[3]}</Card.Text>
          <Card.Text className="history-entry">{userHistory[4]}</Card.Text>
          <Card.Text className="history-entry">{userHistory[5]}</Card.Text>
          <Card.Text className="history-entry">{userHistory[6]}</Card.Text>
          <Card.Text className="history-entry">{userHistory[7]}</Card.Text>
          <Card.Text className="history-entry">{userHistory[8]}</Card.Text>
          <Card.Text className="history-entry">{userHistory[9]}</Card.Text>
        </>
      );
    }
  }

  // Function for the "clear history"-button
  function clearButton() {
    dispatch(clearHistory());
    addTranslation(username, []);
  }

  return (
    <>
      <Card className="history-card">
        <Card.Title className="history-title">Translation History</Card.Title>
        <Card.Body id="card-body">{printHistory()}</Card.Body>
        {userHistory.length > 0 && <Button id="clear-button" onClick={clearButton}> Clear History</Button> }
      </Card>
    </>
  );
};

export default TranslationHistory;
