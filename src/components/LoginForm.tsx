import { useForm } from "react-hook-form";
import "../styles/LoginForm.css";
import { useDispatch } from "react-redux";
import { login } from "../store/userSlice";
import { useNavigate } from "react-router-dom";
import { checkUserByName, createNewUSer } from "../utils/userHandling";

interface Props {
  width: number;
  height: number;
}

export const LoginForm: React.FC<Props> = (props: Props) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const { register, handleSubmit } = useForm();

  const onSubmit = (data: any): void => {
    if (checkUserByName(data.input)) {
      dispatch(login({ username: data.input, history: [] }));
      navigate("/translate");
    } else {
      createNewUSer(data.input, []);
      dispatch(login({ username: data.input, history: [] }));
      navigate("/translate");
    }
  };

  const keyDownHandler = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.code === "Enter") {
      handleSubmit(onSubmit);
    }
  };

  return (
    <div style={{ width: `${props.width}em`, height: `${props.height}em` }}>
      <form onSubmit={handleSubmit(onSubmit)}>
        <fieldset
          style={{ width: `${props.width}em`, height: `${props.height}em` }}
        >
          <label>Username:</label>
          <input
            type="text"
            placeholder="Enter username..."
            {...register("input")}
            onKeyDown={keyDownHandler}
            style={{
              width: `${props.width - 2 * props.height - 2.5}em`,
              height: `${props.height - 0.2}em`,
            }}
            required
          />
          <button
            type="submit"
            style={{
              width: `${props.height - 0.2}em`,
              height: `${props.height - 0.2}em`,
            }}
          >
            <img
              src="enterInput.png"
              alt="Click to submit"
              style={{
                width: `${props.height - 0.2}em`,
                height: `${props.height - 0.2}em`,
              }}
            />
          </button>
        </fieldset>
      </form>
    </div>
  );
};

export default LoginForm;
