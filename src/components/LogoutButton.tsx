import React from "react";
import { Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import "../styles/LogoutButton.css";
import { useDispatch } from "react-redux";
import { logout } from "../store/userSlice";

export const LogoutButton = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const handleLogout = () => {
    dispatch(logout());
    navigate("/login");
  };

  return (
    <Button
      className="logout-btn"
      variant="danger"
      value="Log Out"
      onClick={handleLogout}
    >
      Log Out
    </Button>
  );
};
