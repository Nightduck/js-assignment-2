import { useForm } from "react-hook-form";
import "../styles/TranslationForm.css";
import { useDispatch, useSelector } from "react-redux";
import { updateHistory } from "../store/userSlice";
import { addTranslation } from "../utils/userHandling";

interface Props {
  width: number;
  height: number;
  onSubmit?: Function;
}

export const TranslationForm: React.FC<Props> = (props: Props) => {
  const userHistory = useSelector((state: any) => state.userReducer.history);
  const username = useSelector((state: any) => state.userReducer.username);
  const dispatch = useDispatch();

  const { register, handleSubmit } = useForm();

  const onSubmit = (data: any): void => {
    dispatch(updateHistory([data.input]));
    let translations = userHistory.concat(data.input);
    if (translations.length > 10) {
      translations.shift();
    }
    addTranslation(username, translations);
    (document.getElementById("translate-form") as HTMLFormElement).reset();
  };

  const keyDownHandler = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.code === "Enter") {
      handleSubmit(onSubmit);
    }
  };

  return (
    <div
      style={{ width: `${props.width}em`, height: `${props.height}em` }}
      id="form-container"
    >
      <form id="translate-form" onSubmit={handleSubmit(onSubmit)}>
        <fieldset
          style={{ width: `${props.width}em`, height: `${props.height}em` }}
        >
          <label>
            <img
              src="keyboard.jpg"
              alt="=>"
              style={{
                width: `${props.height - 0.2}em`,
                height: `${props.height - 0.2}em`,
              }}
            />
          </label>
          <input
            id="input"
            type="text"
            placeholder="Enter word for translation"
            {...register("input")}
            onKeyDown={keyDownHandler}
            style={{
              width: `${props.width - 2 * props.height - 0.2}em`,
              height: `${props.height - 0.2}em`,
            }}
            required
          />
          <button
            type="submit"
            style={{
              width: `${props.height - 0.2}em`,
              height: `${props.height - 0.2}em`,
            }}
          >
            <img
              src="enterInput.png"
              alt="Click to submit"
              style={{
                width: `${props.height - 0.2}em`,
                height: `${props.height - 0.2}em`,
              }}
            />
          </button>
        </fieldset>
      </form>
    </div>
  );
};

export default TranslationForm;
