import React from "react";
import Card from "react-bootstrap/esm/Card";
import Image from "react-bootstrap/Image";
import logo from "../logo.svg";
import "../styles/NavbarUser.css";

interface Props {
  user?: string;
}

export const NavbarUser: React.FC<Props> = (props: Props) => {
  return (
    <>
      <div className="user-container">
        <Image className="image" roundedCircle src={logo} />
        <Card className="user-card" text="light">
          <Card.Text className="username">{props.user}</Card.Text>
        </Card>
      </div>
    </>
  );
};
