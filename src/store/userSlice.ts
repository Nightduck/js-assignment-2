import {  createSlice, current } from "@reduxjs/toolkit";
import { stringify } from "querystring";
import { useReducer } from "react";
import { useSelector } from "react-redux"
import { store } from "./store";

const initialState = {
    username: null,
    history: []
} 

export const userSlice = createSlice({
    name: "user",
    initialState,
    reducers: {
        login: (state, action): void =>{
            state.username = action.payload.username;
            state.history = action.payload.history;
        },
        logout: (state): void => {
            state.username = null;
            state.history = [];
        },
        updateHistory: (state, action): void =>{
            state.history = state.history.concat(action.payload);
            if (state.history.length > 10) {
                state.history  = state.history.slice(state.history.length-10);
            }
        },
        clearHistory: (state): void =>{
            state.history = [];
        }
    }
});

export const { login, logout, updateHistory, clearHistory } = userSlice.actions;
export default userSlice.reducer;
