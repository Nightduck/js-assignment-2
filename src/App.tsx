import { PageNavbar } from "./components/PageNavbar";
import "bootstrap/dist/css/bootstrap.min.css";
import { TranslationHistory } from "./components/TranslationHistory";
import { store } from "./store/store";
import { Provider } from "react-redux";

import { Login } from "./routes/Login";

function App() {
  return (
    <div className="App">
      <Provider store={store}>
        <PageNavbar />
        <Login />
        <TranslationHistory />
      </Provider>
    </div>
  );
}

export default App;
