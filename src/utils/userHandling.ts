import { getById, getByUsername, clearResponsLog, addNewUser, updateUserHistoryById, getRespons } from "./userApi"

export const checkUserById = (id: number) => {
    try{
        getById(id);
        const result = getRespons();
        let obj = Object.values(result[0]);
        clearResponsLog();
        if(obj[0].length){
            return true;    
        }

    }
    catch{
        return false;
    }
}

export const checkUserByName = (username: string) => {
    try{
        getByUsername(username);
        let respons = getRespons()
        clearResponsLog();
        let obj = Object.values(respons[0]);
        if(obj[0].length > 0){
            return true;
        }
    }
    catch{
        return false;
    }
}

export const createNewUSer = (username: string, history: string[]) => {
    try{
        addNewUser(username, history);
        clearResponsLog();        
    }
    catch(error){
        console.log(error);
    }
}

const getIdByUsername = (username: string) => {
    try{
        getByUsername(username);
        let respons = getRespons();
        let obj = Object.values(respons[0]);
        clearResponsLog();
        return obj[2];
    }
    catch(error){
        console.log(error);
    }
}

export const addTranslation = (username: string, translation: string[]) => {
    try{
        const id: number = getIdByUsername(username);
        updateUserHistoryById(id, translation);
        clearResponsLog();
    }
    catch(error){
        console.log(error);
        
    }
}