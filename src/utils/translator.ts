
export const stringToSigns = (input: string): string[] => {
    let signs: string[] = [];
    let easterEgg: string[] = [];
    for (let letter of input.split("")) {
        if(letter === " "){
            continue;
        }
        else {
            signs.push(`individial_signs/${letter.toLowerCase()}.png`);
        }
    }
    if(input.toLowerCase() === "ok") {
        easterEgg.push(`individial_signs/eastereggOK.png`);
        return easterEgg;
    }
    else {
        return signs;
    }
}

export default stringToSigns