
const API_URL: string = "https://noroff-lit-api.herokuapp.com/translations";
const API_KEY: string = "90ef56c6-a219-43a6-bbdd-d5cbfd968135";
let response: object[] = [];

const addToResponse = (result: any) => {
    if(result[0] !== undefined && result[0] !== null){
        response.push(result[0]);
    }
}

export const getRespons = (): object[] => {
    return response;
}

export const clearResponsLog = () => {
    response = [];
}

export const getById = async (id: number) => {

    await fetch(`${API_URL}/?id=${id}`)
        .then(response => response.json())
        .then(results => {
            addToResponse(results)
        })
        .catch(error => 
            console.log(error)
        );
}

export const getByUsername = async (username: string) => {

    await fetch(`${API_URL}/?username=${username}`)
        .then(response => response.json())
        .then(results => {
            addToResponse(results)
        })
        .catch(error => 
            console.log(error)
        )
} 

export const addNewUser = async (username: string, history: string[]) => {

    await fetch(`${API_URL}/`, {
        method: 'POST',
        headers: {
          'X-API-Key': API_KEY, // API_KEY?
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ 
            username: username, 
            translations: history 
        })
    })
    .then(response => {
      if (!response.ok) {
        throw new Error('Could not create new user')
      }
      return response.json()
    })
    .then(newUser => {
      addToResponse(newUser);
    })
    .catch(error => 
        console.log(error)
        
    )
}

export const updateUserHistoryById = async (id: number, history: string[]) => {

    await fetch(`${API_URL}/${id}`, {
        method: 'PATCH', // NB: Set method to PATCH
        headers: {
            'X-API-Key': API_KEY,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            // Provide new translations to add to user with id 
            translations: history
        })
    })
    .then(response => {
      if (!response.ok) {
        throw new Error('Could not update translations history')
      }
      return response.json()
    })
    .then(updatedUser => 
      addToResponse(updatedUser)
    )
    .catch(error => 
        console.log(error)
    )
}